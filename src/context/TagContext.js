import React, { createContext, useState } from 'react';

const TagContext = createContext({});

export const TagContextProvider = ({ children }) => {
  const [selectedTags, setSelectedTags] = useState([]);
  return (
    <TagContext.Provider value={{ selectedTags, setSelectedTags }}>
      {children}
    </TagContext.Provider>
  );
};

export default TagContext;
