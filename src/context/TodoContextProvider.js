import React, { useState, createContext } from 'react';
import todoData from '../data/data';
const TodoContext = createContext({});

export const TodoContextProvider = ({ children }) => {
  const [todos, setTodos] = useState(todoData);
  return (
    <TodoContext.Provider value={{ todos, setTodos }}>
      {children}
    </TodoContext.Provider>
  );
};

export default TodoContext;
