import React from 'react';
import AddTodo from 'components/AddTodo/AddTodo';
import Filter from 'components/FilterBox/FilterBox';
import TodoList from 'components/TodoList/TodoList';
import { TodoContextProvider, TagContextProvider } from 'context';
import './Home.css';

const Home = () => {
  return (
    <div className='home'>
      <TodoContextProvider>
        <AddTodo />
        <TagContextProvider>
          <Filter />
          <TodoList />
        </TagContextProvider>
      </TodoContextProvider>
    </div>
  );
};
export default Home;
