const todoData = [
  {
    id: 1,
    txt: 'Learn React',
    tags: ['react', 'js'],
    addedDate: '2022-06-29 11:32',
    doneDate: '',
    delDate: '',
  },
  {
    id: 2,
    txt: 'Learn JS',
    tags: ['todo', 'js'],
    addedDate: '2022-06-29 11:32',
    doneDate: '',
    delDate: '',
  },
  {
    id: 3,
    txt: 'Learn Redux',
    tags: ['todo', 'redux', 'js', 'react'],
    addedDate: '2022-06-29 11:32',
    doneDate: '',
    delDate: '',
  },
  {
    id: 4,
    txt: 'Refactoring the project',
    tags: ['project', 'refactoring', 'java'],
    addedDate: '2022-06-29 11:32',
    doneDate: '',
    delDate: '',
  },
  {
    id: 5,
    txt: 'Learn Spring Boot',
    tags: ['spring', 'boot', 'java'],
    addedDate: '2022-06-29 11:32',
    doneDate: '',
    delDate: '',
  },
  {
    id: 6,
    txt: 'Learn Concurrency API',
    tags: ['java', 'thread'],
    addedDate: '2022-07-02 20:29',
    doneDate: '',
    delDate: '',
  },
];

export default todoData;
