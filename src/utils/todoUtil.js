export const filterTodos = (todos, selectedTags) => {
  return todos
    .filter((todo) => {
      if (selectedTags.length > 0) {
        for (let selectedTag of selectedTags) {
          if (todo.tags.includes(selectedTag)) {
            return true;
          }
        }
        return false;
      }
      return true;
    })
    .sort((t1, t2) => t2.id - t1.id);
};
