export const tagsFilter = (tags, tag) => {
  //If in searchParams-tags then remove, otherwise add
  tags.includes(tag) ? (tags = tags.filter((n) => n !== tag)) : tags.push(tag);
  return tags;
};

export const tagsStateFilter = (stateTags, urlTags) => {
  //If in searchParams-tags then remove, otherwise add
  urlTags.forEach((tag) => tagsFilter(stateTags, tag));
  return urlTags;
};

export const searchTags = (tags, tag, navigate) => {
  const tagsArr = tagsFilter(tags, tag);

  //If there are no tags, redirect to home page
  if (tagsArr.length > 0) {
    navigate({
      search: `tags=${tagsArr.join(',')}`,
    });
  } else navigate('../');
};

// Get array of given param values
export const tagList = (searchParams, param) => {
  return searchParams.get(param)?.split(',') ?? [];
};

// Select all todo tags and collect as an array
const allTags = (todos) => {
  return [].concat.apply(
    [],
    todos.map((todo) => {
      return todo.tags;
    })
  );
};

// Get distinct tags
export const getUniqueTags = (todos) => {
  const flatTags = allTags(todos);
  const uniqueTags = [];
  flatTags.filter((element) => {
    const isDuplicate = uniqueTags.includes(element);
    if (!isDuplicate) {
      uniqueTags.push(element);
      return true;
    }
    return false;
  });
  return uniqueTags;
};
