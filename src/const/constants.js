export const INITIAL_TODO_STATE = {
  id: 0,
  txt: '',
  tags: [],
  addedDate: '',
  doneDate: '',
  delDate: '',
};
