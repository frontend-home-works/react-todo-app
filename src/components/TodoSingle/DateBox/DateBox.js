import { MdBookmarkAdd, MdBookmarkAdded } from 'react-icons/md';
import styles from './DateBox.module.css';
const DateBox = ({ addedDate, doneDate }) => {
  return (
    <div className={styles.date}>
      <MdBookmarkAdd style={{ color: '#16A085' }} title='Add date' />
      <small>{addedDate}</small>
      {'  '}
      {doneDate ? (
        <>
          <MdBookmarkAdded style={{ color: '#81C784' }} title='Done date' />
          <small>{doneDate}</small>
        </>
      ) : (
        ''
      )}
    </div>
  );
};

export default DateBox;
