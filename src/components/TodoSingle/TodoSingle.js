import React from 'react';
import uuid from 'react-uuid';
import styles from './TodoSingle.module.css';
import TagButton from 'components/TagButton/TagButton';
import TodoActionButtons from 'components/TodoActionButtons/TodoActionButtons';
import DateBox from './DateBox/DateBox';

const TodoSingle = ({ todo, doneBtnHandler, delBtnHandler }) => {
  const { txt, addDate, doneDate, tags } = todo;
  return (
    <div className={styles.todoItem}>
      <div className={styles.left}>
        <DateBox addedDate={addDate} doneDate={doneDate} />
        <div className={styles.todoTxt}>{doneDate ? <s>{txt}</s> : txt}</div>
        <div className={styles.tag}>
          {tags.map((tag) => (
            <TagButton tag={tag} key={uuid()} />
          ))}
        </div>
      </div>
      <div className={styles.right}>
        <TodoActionButtons
          doneDate={doneDate}
          doneBtnHandler={doneBtnHandler}
          delBtnHandler={delBtnHandler}
        />
      </div>
    </div>
  );
};
export default TodoSingle;
