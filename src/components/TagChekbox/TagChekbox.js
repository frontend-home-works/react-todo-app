const TagCheckBox = ({ tag, tags, changeHandler }) => {
  return (
    <div key={tag}>
      <input
        type='checkbox'
        value={tag}
        onChange={changeHandler}
        checked={tags.includes(tag)}
      />
      {tag}
    </div>
  );
};

export default TagCheckBox;
