import React from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import * as tagUtil from 'utils/tagUtil';
const TagButton = ({ tag }) => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const tags = tagUtil.tagList(searchParams, 'tags');

  const btnStyle = () => {
    return {
      backgroundColor: tags.includes(tag) ? 'yellow' : '',
    };
  };

  return (
    <button
      onClick={() => {
        tagUtil.searchTags(tags, tag, navigate);
      }}
      style={btnStyle()}
    >
      {tag}
    </button>
  );
};

export default TagButton;
