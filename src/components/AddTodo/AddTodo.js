import React from 'react';
import { useContext, useState } from 'react';
import * as constants from 'const/constants';
import { TodoContext } from 'context';
import Button from 'components/Button/Button';
import * as helper from './AddTodo.helper';
import styles from './AddTodo.module.css';

const AddTodo = () => {
  const [todo, setTodo] = useState(constants.INITIAL_TODO_STATE);
  // @ts-ignore
  const { todos, setTodos } = useContext(TodoContext);

  const onTxtChangeHandler = (e) => {
    helper.onTxtChange(e, todo, setTodo);
  };

  const onTagsChangeHandler = (e) => {
    helper.onTagsChange(e, todo, setTodo);
  };

  const addBtnClickHandler = () => {
    helper.addTodo(todo, setTodo, todos, setTodos);
  };

  return (
    <>
      <h1 className={styles.form_title}>What whould you like to do next?</h1>
      <input
        className={styles.form_field}
        type='text'
        placeholder='Add Todo'
        value={todo.txt}
        onChange={onTxtChangeHandler}
      />
      <input
        className={styles.form_field}
        type='text'
        placeholder='Tags (comma separated)'
        value={todo.tags}
        onChange={onTagsChangeHandler}
      />
      <Button
        btnTxt={'Submit'}
        clickHandler={addBtnClickHandler}
        btnCls={'cyan'}
      />
    </>
  );
};

export default AddTodo;
