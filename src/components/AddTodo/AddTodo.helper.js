import format from 'date-fns/format';
import { INITIAL_TODO_STATE } from 'const/constants';

export const addTodo = (todo, setTodo, todos, setTodos) => {
  if (todo.txt) {
    // Get max id from todo list
    let lastIdx = Math.max(...todos.map((t) => t.id));
    const newTodo = { ...todo };
    newTodo.addedDate = format(Date.now(), 'yyyy-MM-dd HH:mm');
    newTodo.id = ++lastIdx;
    const newTodos = [...todos];
    newTodos.push(newTodo);
    setTodo(INITIAL_TODO_STATE);
    setTodos(newTodos);
  }
};

export const onTxtChange = (e, todo, setTodo) => {
  const newTodo = { ...todo };
  newTodo.txt = e.target.value;
  setTodo(newTodo);
};
export const onTagsChange = (e, todo, setTodo) => {
  const newTodo = { ...todo };
  newTodo.tags = e.target.value.split(',').map((t) => t.trim());
  setTodo(newTodo);
};
