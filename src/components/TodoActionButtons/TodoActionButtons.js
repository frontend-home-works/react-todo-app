import Button from 'components/Button/Button';

const TodoActionButtons = ({ doneDate, doneBtnHandler, delBtnHandler }) => {
  return (
    <>
      <Button
        btnTxt={doneDate ? 'Undo' : 'Done'}
        btnCls={doneDate ? 'cyan' : 'green'}
        clickHandler={doneBtnHandler}
      />
      <Button btnTxt={'Delete'} btnCls={'red'} clickHandler={delBtnHandler} />
    </>
  );
};
export default TodoActionButtons;
