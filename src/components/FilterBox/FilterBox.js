import React, { useContext, useEffect } from 'react';
import TagContext from 'context/TagContext';
import { TodoContext } from 'context';
import TagCheckBox from 'components/TagChekbox/TagChekbox';
import styles from './FilterBox.module.css';
import { useNavigate, useSearchParams } from 'react-router-dom';
import * as tagUtil from 'utils/tagUtil';

const Filter = () => {
  // @ts-ignore
  const { selectedTags, setSelectedTags } = useContext(TagContext);
  // @ts-ignore
  const { todos } = useContext(TodoContext);
  const [searchParams] = useSearchParams();
  let navigate = useNavigate();
  const uniqueTags = tagUtil.getUniqueTags(todos);
  const tags = tagUtil.tagList(searchParams, 'tags');

  // Set selected tags by changing tag params
  useEffect(() => {
    const newTags = tagUtil.tagsStateFilter(selectedTags, tags);
    setSelectedTags(newTags);
  }, [searchParams, todos]);

  const tagChangeHandler = (tag) => {
    tagUtil.searchTags(tags, tag, navigate);
  };

  return (
    <div className={styles.filterLine}>
      {uniqueTags.map((tag) => (
        <TagCheckBox
          tag={tag}
          tags={tags}
          changeHandler={() => tagChangeHandler(tag)}
          key={tag}
        />
      ))}
    </div>
  );
};
export default Filter;
