import styles from './Button.module.css';

const Button = ({ btnTxt, clickHandler, btnCls }) => {
  return (
    <button
      type='submit'
      onClick={clickHandler}
      className={styles.button + ' ' + styles[btnCls]}
    >
      {btnTxt}
    </button>
  );
};

export default Button;
