import format from 'date-fns/format';

export const doneUndone = (todos, id) => {
  const altered = todos.map((todo) => {
    if (todo.id === id) {
      todo.doneDate = todo.doneDate
        ? ''
        : format(Date.now(), 'yyyy-MM-dd HH:mm');
    }
    return todo;
  });
  return altered;
};
