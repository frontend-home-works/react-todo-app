import TodoSingle from 'components/TodoSingle/TodoSingle';
import { useContext } from 'react';
import { TodoContext, TagContext } from 'context';
import * as helper from './TodoList.helper';
import * as todoUtil from 'utils/todoUtil';

const TodoList = () => {
  // @ts-ignore
  const { todos, setTodos } = useContext(TodoContext);
  // @ts-ignore
  const { selectedTags } = useContext(TagContext);

  const doneBtnHandler = (id) => {
    const altered = helper.doneUndone(todos, id);
    setTodos(altered);
  };

  const delBtnHandler = (id) => {
    const deleted = todos.find((t) => t.id === id);
    if (deleted) {
      const altered = todos.filter((todo) => todo.id !== id);
      setTodos(altered);
    }
  };

  const filteredTodos = todoUtil.filterTodos(todos, selectedTags);

  return (
    <div>
      {filteredTodos.map((todo) => {
        return (
          <TodoSingle
            key={todo.id}
            todo={{
              txt: todo.txt,
              addDate: todo.addedDate,
              doneDate: todo.doneDate,
              tags: todo.tags,
            }}
            doneBtnHandler={() => doneBtnHandler(todo.id)}
            delBtnHandler={() => delBtnHandler(todo.id)}
          />
        );
      })}
    </div>
  );
};

export default TodoList;
